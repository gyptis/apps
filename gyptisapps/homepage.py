#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
import os

from app import app
from dash import dcc, html

from apps import coreshell, grating
from gyptisapps import base_url


def rst2md(s):
    return os.popen(f"echo '{s}' | pandoc -f rst -t markdown").read()


apps_list = [grating, coreshell]


page = []

for _app in apps_list:
    div = []
    div.append(dcc.Markdown("###" + rst2md(_app.__doc__)))
    div.append(html.Br())
    div.append(
        html.Div(html.Img(src=app.get_asset_url(_app.__screenshot__), width="400px"))
    )
    div.append(html.Br())
    div.append(
        dcc.Link(
            html.Button("Launch App", style={"width": "140px"}),
            href=f"{base_url}apps/{_app.__name__.split('.')[-1]}",
        )
    )
    div.append(html.Br())
    div.append(html.Hr())
    page.append(html.Div(div, className="appdescription"))

layout = html.Div(page, className="homepage")
