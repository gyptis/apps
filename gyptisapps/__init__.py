#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT


__version__ = "0.0.1"

try:
    from jupyter_server import serverapp

    servers = list(serverapp.list_running_servers())
    base_url = servers[0]["base_url"]
    base_url += "proxys/"
except:
    base_url = "/"
