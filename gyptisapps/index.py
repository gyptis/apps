#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
import sys

import homepage
from app import app
from dash import dcc, html
from dash.dependencies import Input, Output

from apps import coreshell, grating
from gyptisapps import base_url

debug = bool(int(sys.argv[1]))
host = sys.argv[2]
port = sys.argv[3]


app.layout = html.Div(
    [dcc.Location(id="url", refresh=False), html.Div(id="page-content")]
)


@app.callback(Output("page-content", "children"), Input("url", "pathname"))
def display_page(pathname):
    if pathname == f"{base_url}apps/grating":
        return grating.layout
    elif pathname == f"{base_url}apps/coreshell":
        return coreshell.layout
    elif pathname == f"{base_url}":
        return homepage.layout
    else:
        return "404"


if __name__ == "__main__":
    app.run_server(
        debug=debug,
        host=host,
        port=port,
        threaded=False,
    )
