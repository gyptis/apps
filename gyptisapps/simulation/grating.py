# -*- coding: utf-8 -*-

from collections import OrderedDict

# import matplotlib.pyplot as plt
import numpy as np
from gyptis import Grating, Layered, PlaneWave

degree = 2


def simulation(
    period, R, wavelength, angle, polarization, eps_rod, eps_sub, eps_host, pmesh
):
    thicknesses = OrderedDict(
        {
            "pml_bottom": wavelength,
            "substrate": period * 3,
            "groove": 2 * R * 1.5,
            "superstrate": period * 3,
            "pml_top": wavelength,
        }
    )
    pmesh_rod = pmesh * 1
    mesh_param = dict(
        {
            "pml_bottom": 0.7 * pmesh * abs(eps_sub) ** 0.5,
            "substrate": pmesh * abs(eps_sub) ** 0.5,
            "groove": pmesh,
            "rod": pmesh_rod * abs(eps_rod.real) ** 0.5,
            "superstrate": pmesh,
            "pml_top": 0.7 * pmesh,
        }
    )
    geom = Layered(2, period, thicknesses)
    groove = geom.layers["groove"]
    y0 = geom.y_position["groove"] + thicknesses["groove"] / 2
    rod = geom.add_circle(0, y0, 0, R)
    rod, groove = geom.fragment(groove, rod)
    geom.add_physical(rod, "rod")
    geom.add_physical(groove, "groove")
    mesh_size = {d: wavelength / param for d, param in mesh_param.items()}
    geom.set_mesh_size(mesh_size)
    geom.build(finalize=False)  ## no finalize: this is for debugging the app
    domains = geom.subdomains["surfaces"]
    epsilon = {d: 1 for d in domains}
    epsilon["rod"] = eps_rod
    epsilon["groove"] = eps_host
    epsilon["substrate"] = eps_sub
    mu = {d: 1 for d in domains}
    angle_ = (angle) * np.pi / 180
    pw = PlaneWave(wavelength, angle_, dim=2)
    grating = Grating(
        geom, epsilon, mu, source=pw, polarization=polarization, degree=degree
    )
    return grating


if __name__ == "__main__":
    from gyptis.utils import project_iterative

    period = 800  # period of the grating
    R = 300  # semi axes of the elliptical rods along x and y
    wavelength = 600
    angle = 0  # in degrees
    polarization = "TM"
    eps_rod = 5 - 0.1j
    eps_sub = 2
    eps_host = 2
    pmesh = 9
    g = simulation(
        period, R, wavelength, angle, polarization, eps_rod, eps_sub, eps_host, pmesh
    )
    g.solve()

    u = g.solution["total"]
    uplt = project_iterative(u.real, g.formulation.real_function_space)

    # import plotly.graph_objects as go
    # from fenics_plotly import plot as fpplot
    # from gyptis.utils import project_iterative

    # from gyptisapps.core.io import *
    # camera = dict(up=dict(x=0, y=1, z=0), eye=dict(x=0.0, y=0.0, z=25))
    # cs = [[0, "rgb(0, 0, 0)"], [0.5, "rgb(127, 127, 127)"], [1.0, "rgb(255, 255, 255)"]]
    # # out_geo = fpplot(g.geometry.markers, colorscale=cs, show=False, wireframe=True)

    # from fenics_plotly.fenics_plotly import _get_triangles

    # def _surface_plot_function(
    #     function, colorscale, showscale=True, intensitymode="vertex", **kwargs
    # ):
    #     mesh = function.function_space().mesh()
    #     if intensitymode == "vertex":
    #         val = function.compute_vertex_values()
    #         triangle = _get_triangles(mesh)
    #     else:
    #         # Spooky!
    #         val = function.vector().get_local()
    #         triangle = mesh.cells().T
    #     coord = mesh.coordinates()

    #     hoverinfo = ["val:" + "%.5f" % item for item in val]

    #     if len(coord[0, :]) == 2:
    #         coord = np.c_[coord, np.zeros(len(coord[:, 0]))]
    #     print(coord.shape)
    #     print(triangle.shape)
    #     print(val.shape)

    #     surface = go.Contour(
    #         x=coord[:, 0],
    #         y=coord[:, 1],
    #         z=val,
    #         colorscale=colorscale,
    #     )
    #     return surface

    # out = fpplot(
    #     uplt,
    #     show=False,
    #     colorscale="rdbu_r",
    #     wireframe=False,
    # )
    # fig = out.figure

    # t = np.linspace(0, 2 * np.pi, 100)
    # y0 = g.geometry.thicknesses["groove"] / 2
    # x = R * np.cos(t)
    # y = R * np.sin(t) + y0
    # z = np.zeros_like(t)
    # circ = go.Scatter3d(x=x, y=y, z=z, line=dict(color="black", width=4), mode="lines")

    # for ysub in g.geometry.y_position.values():
    #     # ysub = g.geometry.y_position["substrate"]
    #     subline = go.Scatter3d(
    #         x=[-period / 2, period / 2],
    #         y=[ysub, ysub],
    #         z=[0, 0],
    #         line=dict(color="black", width=4),
    #         mode="lines",
    #     )

    #     fig.add_trace(subline)
    # fig.add_trace(circ)
    # fig.update_layout(
    #     scene_camera=camera,
    #     autosize=False,
    #     showlegend=False,
    #     # width=500,
    #     height=1000,
    # )
    # fig.update_yaxes(
    #     scaleanchor="x",
    #     scaleratio=1,
    # )
    # # fig.show()
    # fig.show(config={"modeBarButtonsToRemove": ["zoom", "pan"]})
