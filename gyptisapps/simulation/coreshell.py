# -*- coding: utf-8 -*-


import numpy as np
from gyptis import BoxPML, PlaneWave, Scattering

degree = 2


def simulation(R1, R2, wavelength, angle, polarization, eps1, eps2, pmesh):
    Rcalc = 2 * R2
    lmin = wavelength / pmesh
    n1 = abs(eps1.real) ** 0.5
    n2 = abs(eps2.real) ** 0.5
    pml_width = wavelength

    lbox = Rcalc * 2 * 1.1
    geom = BoxPML(
        dim=2,
        box_size=(lbox, lbox),
        pml_width=(pml_width, pml_width),
        Rcalc=Rcalc,
    )
    box = geom.box
    shell = geom.add_circle(0, 0, 0, R2)
    out = geom.fragment(shell, box)
    box = out[1:3]
    shell = out[0]
    core = geom.add_circle(0, 0, 0, R1)
    core, shell = geom.fragment(core, shell)
    geom.add_physical(box, "box")
    geom.add_physical(core, "core")
    geom.add_physical(shell, "shell")
    [geom.set_size(pml, lmin * 1) for pml in geom.pmls]
    geom.set_size("box", lmin)
    geom.set_size("core", 0.5 * lmin / n1)
    geom.set_size("shell", 0.5 * lmin / n2)
    geom.build(finalize=True)

    angle_rad = angle * np.pi / 180

    pw = PlaneWave(
        wavelength=wavelength, angle=angle_rad, dim=2, domain=geom.mesh, degree=degree
    )
    epsilon = dict(box=1, core=eps1, shell=eps2)
    simu = Scattering(
        geom,
        epsilon,
        source=pw,
        degree=degree,
        polarization=polarization,
    )
    return simu


if __name__ == "__main__":
    from gyptis.utils import project_iterative

    simu = simulation(1, 2, 1, 0, "TE", 3, 5, 4)
    simu.solve()

    u = simu.solution["total"]
    uplt = project_iterative(u.real, simu.formulation.real_function_space)
    cs = simu.get_cross_sections()
    print("cross sections")
    print("--------------")
    for k, v in cs.items():
        print(f"{k} {v:.3f}nm")
    B = (cs["scattering"] + cs["absorption"]) / cs["extinction"]
    print("energy balance", B)

    # camera = dict(up=dict(x=0, y=1, z=0), eye=dict(x=0.0, y=0.0, z=25))
    # cs = [[0, "rgb(0, 0, 0)"], [0.5, "rgb(127, 127, 127)"], [1.0, "rgb(255, 255, 255)"]]
    # # out_geo = fpplot(g.geometry.markers, colorscale=cs, show=False, wireframe=True)

    # from fenics_plotly.fenics_plotly import _get_triangles

    # def _surface_plot_function(
    #     function, colorscale, showscale=True, intensitymode="vertex", **kwargs
    # ):
    #     mesh = function.function_space().mesh()
    #     if intensitymode == "vertex":
    #         val = function.compute_vertex_values()
    #         triangle = _get_triangles(mesh)
    #     else:
    #         # Spooky!
    #         val = function.vector().get_local()
    #         triangle = mesh.cells().T
    #     coord = mesh.coordinates()

    #     hoverinfo = ["val:" + "%.5f" % item for item in val]

    #     if len(coord[0, :]) == 2:
    #         coord = np.c_[coord, np.zeros(len(coord[:, 0]))]
    #     print(coord.shape)
    #     print(triangle.shape)
    #     print(val.shape)

    #     surface = go.Contour(
    #         x=coord[:, 0],
    #         y=coord[:, 1],
    #         z=val,
    #         colorscale=colorscale,
    #     )
    #     return surface

    # out = fpplot(
    #     uplt,
    #     show=False,
    #     colorscale="rdbu_r",
    #     wireframe=False,
    # )
    # fig = out.figure

    # t = np.linspace(0, 2 * np.pi, 100)
    # y0 = g.geometry.thicknesses["groove"] / 2
    # x = R * np.cos(t)
    # y = R * np.sin(t) + y0
    # z = np.zeros_like(t)
    # circ = go.Scatter3d(x=x, y=y, z=z, line=dict(color="black", width=4), mode="lines")

    # for ysub in g.geometry.y_position.values():
    #     # ysub = g.geometry.y_position["substrate"]
    #     subline = go.Scatter3d(
    #         x=[-period / 2, period / 2],
    #         y=[ysub, ysub],
    #         z=[0, 0],
    #         line=dict(color="black", width=4),
    #         mode="lines",
    #     )

    #     fig.add_trace(subline)
    # fig.add_trace(circ)
    # fig.update_layout(
    #     scene_camera=camera,
    #     autosize=False,
    #     showlegend=False,
    #     # width=500,
    #     height=1000,
    # )
    # fig.update_yaxes(
    #     scaleanchor="x",
    #     scaleratio=1,
    # )
    # # fig.show()
    # fig.show(config={"modeBarButtonsToRemove": ["zoom", "pan"]})
