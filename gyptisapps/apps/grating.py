#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT

"""
Diffraction Grating Simulator
=============================

An application to simulate a diffraction grating.

"""

__screenshot__ = "grating.png"

import os
import tempfile

from app import app
from core.components import *
from core.io import *
from core.viz import *
from gyptis.plot import colors
from simulation.grating import *

tmp_dir = tempfile.mkdtemp()
mesh_data = os.path.join(tmp_dir, "data.txt")
field_data = os.path.join(tmp_dir, "output.hdf5")

geometry_ctrl = html.Div(
    [
        html.H4("Geometry"),
        html.Div(
            [
                "Periodicity: ",
                dcc.Input(
                    id="period-input",
                    debounce=debounce,
                    value=400,
                    type="number",
                    min=0,
                ),
                " nm",
            ]
        ),
        html.Br(),
        html.Div(
            [
                "Radius: ",
                dcc.Input(
                    id="radius-input",
                    debounce=debounce,
                    value=80,
                    type="number",
                    min=0,
                ),
                " nm",
            ]
        ),
        html.P(id="err-radius", style={"color": "red"}),
        html.Br(),
        html.Div(
            [
                "Mesh: ",
                dcc.Slider(
                    id="mesh-slider",
                    min=6,
                    max=16,
                    step=1,
                    value=10,
                    marks={i: f"{i}" for i in range(6, 17)},
                ),
            ],
        ),
        html.Br(),
    ]
)

material_ctrl = html.Div(
    [
        html.H4("Materials"),
        html.Div(
            [
                "Substrate permittivity: ",
                dcc.Input(
                    id="substrate-input",
                    debounce=debounce,
                    value=2.0,
                    type="number",
                ),
            ]
        ),
        html.Br(),
        html.Div(
            [
                "Host permittivity: ",
                dcc.Input(
                    id="host-input",
                    debounce=debounce,
                    value=2.0,
                    type="number",
                ),
            ]
        ),
        html.Br(),
        html.Div(["Wire permittivity: "]),
        html.Div(
            [
                "ϵ = ",
                dcc.Input(
                    id="wire-epsilon-input-real",
                    debounce=debounce,
                    value=-1.75,
                    type="number",
                ),
                " + j ",
                dcc.Input(
                    id="wire-epsilon-input-imag",
                    debounce=debounce,
                    value=-5.4,
                    type="number",
                    max=0,
                ),
            ]
        ),
        html.Br(),
    ]
)

wave_ctrl = html.Div(
    [
        html.H4("Wave"),
        html.Div(
            [
                "Wavelength: ",
                dcc.Input(
                    id="wavelength-input",
                    debounce=debounce,
                    value=441,
                    type="number",
                    min=0,
                ),
                " nm",
            ]
        ),
        html.Br(),
        html.Div(
            [
                "Angle: ",
                dcc.Input(
                    id="angle-input",
                    debounce=debounce,
                    value=20,
                    type="number",
                    min=-90,
                    max=90,
                ),
                " degrees",
            ]
        ),
        html.Br(),
        html.Div(
            [
                "Polarization: ",
                dcc.Dropdown(
                    id="pola-dropdown",
                    options=[
                        {"label": "TE", "value": "TE"},
                        {"label": "TM", "value": "TM"},
                    ],
                    value="TM",
                    style={"width": "100%"},
                ),
            ],
        ),
    ]
)


control = ControlPanel(
    geometry_ctrl, material_ctrl, wave_ctrl, title="Diffraction Grating Simulator"
)

# content = MainContent()

content = html.Div(
    [
        dcc.Graph(id="field"),
        html.Br(),
        dcc.Graph(id="effs"),
    ],
    className="pagecontent",
)

layout = html.Div(
    [control.content(), content, dcc.Store(id="intermediate-value")],
    className="container-fluid",
)


@app.callback(
    Output("err-radius", "children"),
    Input(component_id="period-input", component_property="value"),
    Input(component_id="radius-input", component_property="value"),
)
def check_radius(period, radius):
    if period is None or radius is None:
        # PreventUpdate prevents ALL outputs updating
        raise PreventUpdate
    if (radius) > (period) / 2:
        return f"radius must be smaller than period/2"
    return ""


inputs = [
    Input("run-button", "n_clicks"),
    State("period-input", "value"),
    State("radius-input", "value"),
    State("mesh-slider", "value"),
    State("substrate-input", "value"),
    State("host-input", "value"),
    State("wire-epsilon-input-real", "value"),
    State("wire-epsilon-input-imag", "value"),
    State("wavelength-input", "value"),
    State("angle-input", "value"),
    State("pola-dropdown", "value"),
]
outputs = [Output("field", "figure"), Output("effs", "figure")]


def plot_field(uplt):
    out = fpplot(uplt, show=False, colorscale="rdbu_r", wireframe=False)

    fig_field = out.figure
    camera = dict(up=dict(x=0, y=1, z=0), eye=dict(x=0.0, y=0.0, z=25))

    fig_field.update_layout(
        scene_camera=camera,
        autosize=False,
        showlegend=False,
        # width=500,
        height=700,
        title=dict(text="Field map"),
    )
    fig_field.update_yaxes(
        scaleanchor="x",
        scaleratio=1,
    )
    return fig_field


def plot_efficiencies(effs):
    x = list(effs.keys())
    y = list(effs.values())
    fig_effs = go.Figure(
        data=go.Bar(x=x, y=y, marker_color="rgb" + str(colors["green"]))
    )
    fig_effs.update_layout(
        title=dict(
            text=f"Efficiencies: {x[0]}={y[0]:.3f}, {x[1]}={y[1]:.3f}, {x[2]}={y[2]:.3f}, {x[3]}={y[3]:.3f}"
        )
    )
    return fig_effs


def plot_geo(fig, R, period, y_position, thicknesses):
    t = np.linspace(0, 2 * np.pi, 100)
    y0 = thicknesses["groove"] / 2
    x = R * np.cos(t)
    y = R * np.sin(t) + y0
    z = np.zeros_like(t)
    circ = go.Scatter3d(x=x, y=y, z=z, line=dict(color="black", width=4), mode="lines")

    for yp in y_position.values():
        # ysub = g.geometry.y_position["substrate"]
        subline = go.Scatter3d(
            x=[-period / 2, period / 2],
            y=[yp, yp],
            z=[0, 0],
            line=dict(color="black", width=4),
            mode="lines",
        )

        fig.add_trace(subline)
    fig.add_trace(circ)
    return fig


@app.callback(Output("intermediate-value", "data"), inputs)
def run_simulation(
    nclick,
    period,
    radius,
    pmesh,
    eps_sub,
    eps_host,
    eps_wire_re,
    eps_wire_im,
    wavelength,
    angle,
    pola,
):
    if nclick is None:
        raise PreventUpdate

    g = simulation(
        period,
        radius,
        wavelength,
        angle,
        pola,
        eps_wire_re + 1j * eps_wire_im,
        eps_sub,
        eps_host,
        pmesh,
    )

    g.solve()

    u = g.solution["total"]
    uplt = project_iterative(u.real, g.formulation.real_function_space)
    effs = g.diffraction_efficiencies(3)

    ### save
    save_mesh_info(g, filename=mesh_data)
    save_dolfin_function(uplt, filename=field_data)
    # write_json(effs, "effs.txt")
    return effs, radius, period, g.geometry.y_position, g.geometry.thicknesses


@app.callback(outputs, Input("intermediate-value", "data"))
def render_page_content(val):
    effs, R, period, y_position, thicknesses = val
    ### read
    uplt = read_solution(field_data, mesh_data)
    fig_field = plot_field(uplt)
    plot_geo(fig_field, R, period, y_position, thicknesses)

    # effs = read_json(filename="effs.txt")
    fig_effs = plot_efficiencies(effs)

    return fig_field, fig_effs
