#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT

"""
Core Shell Nanorod
==================

An application to simulate diffraction by a nanorod.
"""

__screenshot__ = "coreshell.png"

import os
import tempfile

from app import app
from core.components import *
from core.io import *
from core.viz import *
from gyptis.plot import colors
from simulation.coreshell import *

tmp_dir = tempfile.mkdtemp()
mesh_data = os.path.join(tmp_dir, "data.txt")
field_data = os.path.join(tmp_dir, "output.hdf5")

geometry_ctrl = html.Div(
    [
        html.H4("Geometry"),
        html.Div(
            [
                "Core radius: ",
                dcc.Input(
                    id="core-radius-input",
                    debounce=debounce,
                    value=550,
                    type="number",
                    min=0,
                ),
                " nm",
            ]
        ),
        html.Br(),
        html.Div(
            [
                "Shell radius: ",
                dcc.Input(
                    id="shell-radius-input",
                    debounce=debounce,
                    value=820,
                    type="number",
                    min=0,
                ),
                " nm",
            ]
        ),
        html.P(id="err-radius", style={"color": "red"}),
        html.Br(),
        html.Div(
            [
                "Mesh: ",
                dcc.Slider(
                    id="mesh-slider",
                    min=6,
                    max=16,
                    step=1,
                    value=10,
                    marks={i: f"{i}" for i in range(6, 17)},
                ),
            ],
        ),
        html.Br(),
    ]
)

material_ctrl = html.Div(
    [
        html.H4("Materials"),
        html.Div(["Core permittivity: "]),
        html.Div(
            [
                "ϵ = ",
                dcc.Input(
                    id="core-eps-input-re",
                    debounce=debounce,
                    value=4,
                    type="number",
                ),
                " + j ",
                dcc.Input(
                    id="core-eps-input-im",
                    debounce=debounce,
                    value=0,
                    type="number",
                    max=0,
                ),
            ]
        ),
        html.Br(),
        html.Div(["Shell permittivity: "]),
        html.Div(
            [
                "ϵ = ",
                dcc.Input(
                    id="shell-eps-input-re",
                    debounce=debounce,
                    value=-1.8,
                    type="number",
                ),
                " + j ",
                dcc.Input(
                    id="shell-eps-input-im",
                    debounce=debounce,
                    value=-0.1,
                    type="number",
                    max=0,
                ),
            ]
        ),
        html.Br(),
    ]
)

wave_ctrl = html.Div(
    [
        html.H4("Wave"),
        html.Div(
            [
                "Wavelength: ",
                dcc.Input(
                    id="wavelength-input",
                    debounce=debounce,
                    value=600,
                    type="number",
                    min=10,
                ),
                " nm",
            ]
        ),
        html.Br(),
        html.Div(
            [
                "Angle: ",
                dcc.Input(
                    id="angle-input",
                    debounce=debounce,
                    value=0,
                    type="number",
                ),
                " degrees",
            ]
        ),
        html.Br(),
        html.Div(
            [
                "Polarization: ",
                dcc.Dropdown(
                    id="pola-dropdown",
                    options=[
                        {"label": "TE", "value": "TE"},
                        {"label": "TM", "value": "TM"},
                    ],
                    value="TE",
                    style={"width": "100%"},
                ),
            ],
        ),
    ]
)


control = ControlPanel(
    geometry_ctrl, material_ctrl, wave_ctrl, title="Core Shell Nanorod Simulator"
)

# content = MainContent()

content = html.Div(
    [
        dcc.Graph(id="field_core_shell"),
        html.Br(),
        dcc.Graph(id="cs"),
    ],
    className="pagecontent",
)

layout = html.Div(
    [control.content(), content, dcc.Store(id="intermediate-value-core-shell")],
    className="container-fluid",
)


@app.callback(
    Output("error-radii", "children"),
    Input(component_id="core-radius-input", component_property="value"),
    Input(component_id="shell-radius-input", component_property="value"),
)
def check_radius(R1, R2):
    if R1 is None or R2 is None:
        # PreventUpdate prevents ALL outputs updating
        raise PreventUpdate
    if R2 < R2:
        return f"radius must be smaller than period/2"
    return ""


inputs = [
    Input("run-button", "n_clicks"),
    State("core-radius-input", "value"),
    State("shell-radius-input", "value"),
    State("mesh-slider", "value"),
    State("core-eps-input-re", "value"),
    State("core-eps-input-im", "value"),
    State("shell-eps-input-re", "value"),
    State("shell-eps-input-im", "value"),
    State("wavelength-input", "value"),
    State("angle-input", "value"),
    State("pola-dropdown", "value"),
]
outputs = [Output("field_core_shell", "figure"), Output("cs", "figure")]


def plot_field(uplt):
    out = fpplot(uplt, show=False, colorscale="rdbu_r", wireframe=False)

    fig_field = out.figure
    camera = dict(up=dict(x=0, y=1, z=0), eye=dict(x=0.0, y=0.0, z=25))

    fig_field.update_layout(
        scene_camera=camera,
        autosize=False,
        showlegend=False,
        # width=500,
        height=700,
        title=dict(text="Field map"),
    )
    fig_field.update_yaxes(
        scaleanchor="x",
        scaleratio=1,
    )
    return fig_field


def plot_scs(cs):
    x = list(cs.keys())
    y = list(cs.values())

    B = (cs["scattering"] + cs["absorption"]) / cs["extinction"]
    fig_cs = go.Figure(data=go.Bar(x=x, y=y, marker_color="rgb" + str(colors["green"])))
    fig_cs.update_layout(
        title=dict(
            text=f"Cross sections: {x[0]}={y[0]:.3f}nm, {x[1]}={y[1]:.3f}nm, {x[2]}={y[2]:.3f}nm, balance={B:.3f}"
        )
    )
    return fig_cs


def plot_geo(fig, R1, R2):
    t = np.linspace(0, 2 * np.pi, 100)
    x = R1 * np.cos(t)
    y = R1 * np.sin(t)
    z = np.zeros_like(t)
    circ1 = go.Scatter3d(x=x, y=y, z=z, line=dict(color="black", width=4), mode="lines")

    x = R2 * np.cos(t)
    y = R2 * np.sin(t)
    z = np.zeros_like(t)
    circ2 = go.Scatter3d(x=x, y=y, z=z, line=dict(color="black", width=4), mode="lines")

    fig.add_trace(circ1)
    fig.add_trace(circ2)
    return fig


@app.callback(Output("intermediate-value-core-shell", "data"), inputs)
def run_simulation_core_shell(
    nclick,
    R1,
    R2,
    pmesh,
    eps1re,
    eps1im,
    eps2re,
    eps2im,
    wavelength,
    angle,
    pola,
):
    if nclick is None:
        raise PreventUpdate
    simu = simulation(
        R1,
        R2,
        wavelength,
        angle,
        pola,
        eps1re + 1j * eps1im,
        eps2re + 1j * eps2im,
        pmesh,
    )

    simu.solve()

    u = simu.solution["total"]
    uplt = project_iterative(u.real, simu.formulation.real_function_space)
    cs = simu.get_cross_sections()
    # cs["balance"] = B
    ### save
    save_mesh_info(simu, filename=mesh_data)
    save_dolfin_function(uplt, filename=field_data)
    # write_json(cs, "cs.txt")
    return cs, R1, R2


@app.callback(outputs, Input("intermediate-value-core-shell", "data"))
def render_page_content(val):
    cs, R1, R2 = val
    ### read
    uplt = read_solution(field_data, mesh_data)
    fig_field = plot_field(uplt)
    plot_geo(fig_field, R1, R2)

    # cs = read_json(filename="cs.txt")
    fig_cs = plot_scs(cs)

    return fig_field, fig_cs
