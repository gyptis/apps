#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT


import json

from gyptis import dolfin as df
from gyptis.mesh import read_mesh


def write_json(info, filename="data.txt"):
    with open(filename, "w") as outfile:
        json.dump(info, outfile)


def read_json(filename="data.txt"):
    with open(filename, "r") as outfile:
        info = json.load(outfile)
    return info


def save_dolfin_function(u, filename="output.hdf5", id="solution", *args):
    hdf5_file = df.HDF5File(df.MPI.comm_world, filename, "w")
    hdf5_file.write(u, id, *args)


def read_dolfin_function(u, filename="output.hdf5", id="solution", *args):
    hdf5_file = df.HDF5File(df.MPI.comm_world, filename, "r")
    hdf5_file.read(u, id, *args)


def save_mesh_info(self, filename="data.txt"):
    mesh_name = self.geometry.mesh_name
    dir_name = self.geometry.data_dir
    info = dict(mesh_name=mesh_name, dir_name=dir_name)
    write_json(info, filename)


def read_mesh_info(filename="data.txt", dim=2):
    info = read_json(filename)
    return read_mesh(
        info["dir_name"] + "/" + info["mesh_name"],
        info["dir_name"],
        info["dir_name"],
        dim,
    )


def read_solution(hdf5="output.hdf5", data="data.txt", dim=2):
    out = read_mesh_info(filename=data, dim=dim)
    mesh = out["mesh"]
    V = df.FunctionSpace(mesh, "CG", 2)
    vh = df.Function(V)
    read_dolfin_function(vh, filename=hdf5)
    return vh
