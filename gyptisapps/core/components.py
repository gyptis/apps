#!/usr/bin/env python
# -*- coding: utf-8 -*-

import dash_bootstrap_components as dbc
from dash import dcc, html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

from .logo import LOGO
from .template import *

debounce = False

gyptis_theme = "https://gyptis.gitlab.io/_static/css/app.css"
# gyptis_theme =  "http://localhost:8000/dev/gyptis/gyptis/docs/_custom/static/css/app.css"

external_stylesheets = [
    dbc.themes.BOOTSTRAP,
    gyptis_theme,
]

logo_div = html.Div(
    [
        html.Br(),
        html.Br(),
        html.Img(src=LOGO, height=40),
        "  made with gyptis",
        # html.Br(),
        # dcc.Link('HOME', href='/'),
    ],
    className="logo",
)


class ControlPanel:
    def __init__(
        self, geometry_ctrl, material_ctrl, wave_ctrl, title="Control", id="ctrl"
    ):
        self.title = title
        self.id = id
        self.geometry_ctrl = geometry_ctrl
        self.material_ctrl = material_ctrl
        self.wave_ctrl = wave_ctrl

    def content(self):
        return html.Div(
            [
                html.H2(self.title),
                html.Br(),
                self.geometry_ctrl,
                self.material_ctrl,
                self.wave_ctrl,
                html.Br(),
                html.Button("RUN", id="run-button"),
                html.Br(),
                logo_div,
            ],
            className="sidebar",
        )


class MainContent:
    def __init__(self, id="main"):
        self.id = id

    def content(self):
        return html.Div(
            [
                dcc.Graph(id="graph"),
            ],
            className="pagecontent",
        )


# @app.callback(
#     Output("err-radius", "children"),
#     Input(component_id="period-input", component_property="value"),
#     Input(component_id="radius-input", component_property="value"),
# )
# def check_radius(period, radius):
#     if period is None or radius is None:
#         # PreventUpdate prevents ALL outputs updating
#         raise dash.exceptions.PreventUpdate
#     if (radius) > (period) / 2:
#         return f"radius must be smaller than period/2"
#     return ""
#
#
# inputs = [
#     Input("run-button", "n_clicks"),
#     State("period-input", "value"),
#     State("radius-input", "value"),
#     State("substrate-input", "value"),
#     State("wire-epsilon-input-real", "value"),
#     State("wire-epsilon-input-imag", "value"),
#     State("wavelength-input", "value"),
#     State("angle-input", "value"),
#     State("pola-dropdown", "value"),
# ]
#
#
# import dolfin as df
# from fenics_plotly import plot as fpplot
# from simu import *
#
#
# # outputs = [Output("page-content", "children"), Output("graph", "figure")]
# outputs = Output("graph", "figure")
#
#
# @app.callback(outputs, inputs)
# def render_page_content(
#     nclick,
#     period,
#     radius,
#     eps_sub,
#     eps_wire_re,
#     eps_wire_im,
#     wavelength,
#     angle,
#     pola,
# ):
#     # s = f"{period}, {radius}, {eps_sub}, {eps_wire_re}, {eps_wire_im}, {wavelength},{angle}, {pola}"
#
#     # g = simulation(
#     #     period, radius, wavelength, angle, pola, eps_wire_re + 1j * eps_wire_im, eps_sub
#     # )
#     # g.solve()
#     #
#     # u = g.solution["total"]
#     # uplt = project_iterative(u.real, g.formulation.real_function_space)
#     #
#     # out = fpplot(uplt, show=False, colorscale="rdbu_r", wireframe=False)
#     # fig = out.figure
#     # camera = dict(up=dict(x=0, y=1, z=0), eye=dict(x=0.0, y=0.0, z=25))
#     #
#     # fig.update_layout(
#     #     scene_camera=camera,
#     #     autosize=False,
#     #     # width=500,
#     #     height=700,
#     # )
#
#     fig = go.Figure(data=go.Bar(y=[2, 3, 1], marker_color="red"))
#
#     return fig
#
#
# if __name__ == "__main__":
#     app.run_server(
#         debug=True,
#         host="127.0.0.1",
#         threaded=False,
#     )
