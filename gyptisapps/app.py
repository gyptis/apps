#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT

import dash

from gyptisapps import base_url
from gyptisapps.core.components import external_stylesheets

app = dash.Dash(
    __name__,
    title="Gyptis Apps",
    suppress_callback_exceptions=True,
    requests_pathname_prefix=base_url,
    external_stylesheets=external_stylesheets,
)
server = app.server
