#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT

import os
import subprocess
import sys

import click

here = os.path.abspath(os.path.dirname(__file__))
index_file = os.path.join(here, "index.py")


@click.option("--debug", is_flag=True, help="Debug mode.")
@click.option("--host", default="localhost", help="Host name.")
@click.option("--port", default="8050", help="Port number.")
@click.command()
def gyptisapps(debug, host, port):
    click.echo("Running App")
    os.system(f"python {index_file} {int(debug)} {host} {port}")


def setup_dash_app():
    return {
        "command": [
            "gyptisapps",
            "--port",
            "{port}",
        ]
    }
