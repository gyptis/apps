
SHELL := /bin/bash

.DEFAULT_GOAL := help

.PHONY: clean lint req doc help dev

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME = gyptisapps
PYTHON_INTERPRETER = python3
HOSTING = gitlab
VERSION=$(shell python3 -c "import gyptisapps; print(gyptisapps.__version__)")
BRANCH=$(shell git branch --show-current)
# URL=$(shell python3 -c "import gyptis; print(gyptis.__website__)")

GITLAB_PROJECT_ID=27753869
GITLAB_GROUP_ID=11118791

ifeq (,$(shell which conda))
HAS_CONDA=False
else
HAS_CONDA=True
endif

ifdef TEST_PARALLEL
TEST_ARGS=-n auto #--dist loadscope
endif


message = @make -s printmessage RULE=${1}

printmessage: 
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/^/---/" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} | grep "\---${RULE}---" \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=0 \
		-v col_on="$$(tput setaf 4)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s ", col_on, -indent, ">>>"; \
		n = split($$3, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i] ; \
		} \
		printf "%s ", col_off; \
		printf "\n"; \
	}' 

#################################################################################
# COMMANDS                                                                      #
#################################################################################


## Run app
run:
	$(call message,${@})
	@gyptisapps

## Run app in debug mode
dev:
	$(call message,${@})
	@gyptisapps --debug
	

## Run app with jupyter notebook
jnb:
	$(call message,${@})
	@jupyter notebook -y &
	@sleep 3
	@xdg-open http://localhost:8888/proxys/


## Kill jupyter
killnb:
	$(call message,${@})
	@kill $$(pgrep jupyter) || echo "No kernels running"


## Clean project
clean:
	$(call message,${@})
	@find . -not -path "./tests/data/*" | grep -E "(\data.txt|\.hdf5|\.pvd|\.xdmf|\.msh|\.pvtu|\.vtu|\.pvd|jitfailure*|tmp|__pycache__|\.pyc|\.pyo$\)" | xargs rm -rf
	@rm -rf .pytest_cache build/ dist/ tmp/ htmlcov/


## Reformat code
style:
	$(call message,${@})
	@isort .
	@black .

## Push to gitlab
gl:
	$(call message,${@})
	@git add -A
	@read -p "Enter commit message: " MSG; \
	git commit -a -m "$$MSG"
	@git push origin $(BRANCH)
	

## Show gitlab repository
repo:
	$(call message,${@})
	xdg-open https://gitlab.com/gyptis/apps


## Clean, reformat and push to gitlab
save: clean style gl
	$(call message,${@})
	

## Push to gitlab (skipping continuous integration)
gl-noci:
	$(call message,${@})
	@git add -A
	@read -p "Enter commit message: " MSG; \
	git commit -a -m "$$MSG [skip ci]"
	@git push origin $(BRANCH)
	
	

## Clean, reformat and push to gitlab (skipping continuous integration)
save-noci: clean style gl-noci
	$(call message,${@})
	

## Tag and push tags
tag: clean style
	$(call message,${@})
	@if [ "$(shell git rev-parse --abbrev-ref HEAD)" != "master" ]; then exit 1; fi
	@echo "Version v$(VERSION)"
	@git add -A
	git commit -a -m "Publish v$(VERSION)"
	@git push origin $(BRANCH)
	@git tag v$(VERSION) && git push --tags || echo Ignoring tag since it already exists
	
## Create a release
release:
	$(call message,${@})
	@if [ "$(shell git rev-parse --abbrev-ref HEAD)" != "master" ]; then exit 1; fi
	@gitlab project-release create --project-id $(GITLAB_PROJECT_ID) \
	--name "version $(VERSION)" --tag-name "v$(VERSION)" --description "Released version $(VERSION)"
                                     

## Create python package
package: setup.py
	$(call message,${@})
	@if [ "$(shell git rev-parse --abbrev-ref HEAD)" != "master" ]; then exit 1; fi
	@rm -f dist/*
	python3 setup.py sdist
	python3 setup.py bdist_wheel --universal

## Upload to pypi
pypi: package
	$(call message,${@})
	@if [ "$(shell git rev-parse --abbrev-ref HEAD)" != "master" ]; then exit 1; fi
	@twine upload dist/*

## Make checksum for release
checksum:
	$(call message,${@})
	@echo v$(VERSION)
	$(eval SHA256 := $(shell curl -sL https://gitlab.com/gyptis/apps/-/archive/v$(VERSION)/apps-v$(VERSION).tar.gz | openssl sha256 | cut  -c10-))
	@echo $(SHA256)



## Publish release on pypi and conda-forge
publish: tag release pypi
	$(call message,${@})

	
#################################################################################
# Self Documenting Commands                                                     #
#################################################################################


# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>

help:
	@echo -e "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo -e
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
